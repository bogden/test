<?php

$x = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

/**
 * @param array $data
 * @return array|null
 */
function strangeArrayModifier($data = [])
{
    if (is_array($data)) {
        $result = [];
        $data = array_reverse($data);
        $needData = &$result;
        foreach ($data as $key => $item) {
            $needData = &$needData[$item];
        }
        return $result;
    }
    return null;
}

$result = strangeArrayModifier($x);
print_r($result);

/*
 * array:1 [▼
 *    "h" => array:1 [▼
 *      "g" => array:1 [▼
 *        "f" => array:1 [▼
 *          "e" => array:1 [▼
 *            "d" => array:1 [▼
 *              "c" => array:1 [▼
 *                "b" => array:1 [▼
 *                  "a" => null
 *                ]
 *              ]
 *            ]
 *          ]
 *        ]
 *      ]
 *    ]
 *  ]
 */

?>