<?php

$data = [
    'parent.child.field' => 1,
    'parent.child.field2' => 2,
    'parent2.child.name' => 'test',
    'parent2.child2.name' => 'test',
    'parent2.child2.position' => 10,
    'parent3.child3.position' => 10,
];

/**
 * @param array $data
 * @return array|null
 */
function strangeArrayParser($data = [])
{
    if (is_array($data)) {
        $result = [];
        foreach ($data as $key => $value) {
            $key = explode('.', $key);
            $needData = &$result;
            for ($i = 0; $i < count($key) - 1; $i++) {
                $needData = &$needData[$key[$i]];
            }
            $needData[$key[$i]] = $value;
        }
        return $result;
    }
    return null;
}

$result = strangeArrayParser($data);
print_r($result);

/** Result:
 * array:3 [▼
 *    "parent" => array:1 [▼
 *      "child" => array:2 [▼
 *        "field" => 1
 *        "field2" => 2
 *      ]
 *    ]
 *    "parent2" => array:2 [▼
 *      "child" => array:1 [▼
 *        "name" => "test"
 *      ]
 *      "child2" => array:2 [▼
 *        "name" => "test"
 *        "position" => 10
 *      ]
 *    ]
 *    "parent3" => array:1 [▼
 *      "child3" => array:1 [▼
 *        "position" => 10
 *      ]
 *    ]
 *  ]
 */

?>